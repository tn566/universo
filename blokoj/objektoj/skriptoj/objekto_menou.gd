extends Control


func _on_Close_button_pressed():
	set_visible(false)
	
	
func _resize(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$MarginContainer.rect_size += event.relative


func _drag(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$MarginContainer.rect_position += event.relative
	

const QueryObject = preload("queries.gd")

var ItemListContent = []


func FillItemList():
	# Заполняет список найдеными продуктами
	for Item in ItemListContent:
		get_node("MarginContainer/VBoxContainer/ItemList").add_item(Item, null, true)


# Вызывается перед появлением окна
func _on_Objekto_draw():
	var q = QueryObject.new()

	# Делаем запрос к бэкэнду
	$HTTPObjectoRequestFind.request(q.URL, Global.backend_headers, true, 2, q.objecto_query())

extends Node2D

var m
var c
var i

var delay1 # переменные вынес из функции _ready в глобальную область видимости, чтобы их можно было использовать в тестах
var delay2
var delay3
var delay4
var delay5
var delay6
var delay7

var rng = RandomNumberGenerator.new()


func _ready():
	rng.randomize()
	delay1 = rng.randi_range(3, 6)
	m = rng.randi_range(1005, 1505)
	c = rng.randi_range(900, 1100)
	i=m*c/1000
	print(delay1)
	print(m)
	print(c)
	deferred_1(delay1)

	delay2 = rng.randi_range(21, 27)+delay1
	print(delay2)
	deferred_2(delay2)

	delay3 = rng.randi_range(9, 15)+delay2
	print(delay3)
	deferred_3(delay3)

	delay4 = rng.randi_range(6, 9)+delay3
	print(delay4)
	deferred_4(delay4)
	
	delay5 = rng.randi_range(21, 27)+delay4
	print(delay5)
	deferred_5(delay5)
	
	delay6 = rng.randi_range(9, 15)+delay5
	print(delay6)
	deferred_6(delay6)
	
	delay7 = rng.randi_range(6, 9)+delay6
	print(delay7)
	deferred_7(delay7)


func deferred_1(delay_time): # изменил название параметра с delay - номер п/п на одно значение, чтобы не было конфликта с глобальными переменными, которые имеют такие же имена
	yield(get_tree().create_timer(delay1), "timeout")
	print("Корабль вылетел со станции!")
	$Label.text ="Корабль вылетел со станции!"

func deferred_2(delay_time):
	yield(get_tree().create_timer(delay2), "timeout")
	print("Корабль подлетел к астероидам!")
	$Label.text ="Корабль подлетел к астероидам!"

func deferred_3(delay_time):
	yield(get_tree().create_timer(delay3), "timeout")
	print("Закончено дробление астероида!")
	$Label.text ="Закончено дробление астероида!"
	
func deferred_4(delay_time):
	yield(get_tree().create_timer(delay4), "timeout")
	print("Корабль собрал минералы, добыто: %s кг" % m)
	$Label.text =("Корабль собрал минералы, добыто: %s кг" % m)
	
func deferred_5(delay_time):
	yield(get_tree().create_timer(delay5), "timeout")
	print("Корабль подлетел к станции!")
	$Label.text ="Корабль подлетел к станции!"
	
func deferred_6(delay_time):
	yield(get_tree().create_timer(delay6), "timeout")
	print("Корабль залетел на станцию!")
	$Label.text ="Корабль залетел на станцию!"
	
func deferred_7(del7):
	yield(get_tree().create_timer(delay7), "timeout")
	print("Минералы проданы за %s Инмо" % i)
	$Label.text =("Минералы проданы за %s Инмо" % i)

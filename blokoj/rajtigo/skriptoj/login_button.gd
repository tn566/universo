extends Button


const QueryObject = preload("queries.gd")

# Объект HTTP запроса
var request
# Объект с данными для запросов
var q


# Вызывается при завершении обработки запроса авторизации к бэкэнду
func login_request_complete(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp: Dictionary = parse_json(resp)
	
	# Если ответ от бэкэнда содержит данные
	if parsed_resp.has('data'):
		# true, если авторизован
		Global.status = parsed_resp['data']['ensaluti']['status']
		
		var message = parsed_resp['data']['ensaluti']['message']
		
		# Если не авторизован, выводим сообщение, которое вернул бэкэнд
		if !Global.status:
			message = tr("Wrong login or password")
			$'../message'.set_mytext(message)
		# Если авторизован
		else:
			# Сюда сложим куки, которые прислал сервер в заголовках ответа
			var cookies = PoolStringArray()
			# Вытаскиваем токен, из ответа
			var token = parsed_resp['data']['ensaluti']['token']
			# Вытаскиваем csrf токен, из ответа
			var csrfToken = parsed_resp['data']['ensaluti']['csrfToken']
			# Вытаскиваем id пользователя из ответа
			Global.id = parsed_resp['data']['ensaluti']['uzanto']['objId']
			
			# Заполняем заголовки для следующих запросов к бэкэнду
			Global.backend_headers.append("Referer: https://t34.tehnokom.su/api/v1.1/")
			Global.backend_headers.append("X-Auth-Token: %s" % token)
			Global.backend_headers.append("X-CSRFToken: %s" % csrfToken)
			
			# Вытаксиваем куки из заголовков ответа
			for h in headers:
				if h.to_lower().begins_with('set-cookie'):
					cookies.append(h.split(':', true, 1)[1].strip_edges().split("; ")[0])
			# Куки так же сохраняем в заголовках для последующих запросов
			Global.backend_headers.append("Cookie: %s" % cookies.join("; "))
			
			# Разрегистрируем обработчик сигнала request_completed (вызывается
			# по завершении HTTPRequest)
			request.disconnect('request_completed', self, 'login_request_complete')
			# Регистрируем новый обработчик (для обработки ответа на запрос по никнейму)
			request.connect('request_completed', self, 'get_nickname_request_complete')
			
			# Делаем запрос к бэкэнду для получения никнейма.
			# Ответ будет обрабатываться в функции get_nickname_request_complete
			var error = request.request(q.URL_DATA, Global.backend_headers, true, 2, q.get_nickname_query(Global.id))
			
			# Если запрос не выполнен из-за какой-то ожибки
			# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
			if error != OK:
				print('Error in GET (nickname) Request.')
	# Если ответ от бэкэнда не содержит данные, которые мы ожидаем, выводим всё тело ответа
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	else:
		print(resp)


# Вызывается при завершении обработки запроса по никнейму к бэкэнду
func get_nickname_request_complete(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	
	# Если ответ от бэкэнда содержит данные
	if parsed_resp.has('data'):
		# Если никнейм не задан
		if len(parsed_resp['data']['universoUzanto']['edges']) == 0:
			# Загружаем сцену с данными профиля для ввода никнейма
			get_tree().change_scene('res://blokoj/profilo/profilo.tscn')
		# Если получили никнейм
		else:
			# Сохраняем данные в глобальном объекте
			Global.nickname = parsed_resp['data']['universoUzanto']['edges'][0]['node']['retnomo']['enhavo']
			Global.nickname_uuid = parsed_resp['data']['universoUzanto']['edges'][0]['node']['uuid']
			
			# Открываем сцену с видом космостанции
			get_tree().change_scene('res://blokoj/kosmostacio/Kosmostacio.tscn')
	# Если ответ от бэкэнда не содержит данные, которые мы ожидаем, выводим всё тело ответа
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	else:
		print(resp)


# Вызывается при нажатии кнопки login
func _pressed():
	var password = $'../Control/your_password'.text
	var login = $'../Control/your_login'.text
	
	# Создаём объект HTTP запроса
	request = HTTPRequest.new()
	# Создаём объект с данными для запросов.
	# Данные для запросов и сами запросы храним в queries.gd
	q = QueryObject.new()
	
	# Добавляем объект запроса в сцену
	add_child(request)
	# Регистрируем обработчик сигнала request_completed, который придёт по завершении запроса
	request.connect('request_completed', self, 'login_request_complete')
	# Делаем запрос авторизации к бэкэнду
	# Ответ будет обрабатываться в функции login_request_complete
	var error = request.request(q.URL_AUTH, Global.backend_headers, true, 2, q.auth_query(login, password))
	
	# Если запрос не выполнен из-за какой-то ожибки
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	if error != OK:
		print('Error in Auth Request.')

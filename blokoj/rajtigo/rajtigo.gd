extends Node2D


# Вызывается при открытии сцены
func _enter_tree():
	# Прячем глобальную сцену с меню
	Title.set_visible(false)
	# Скрываем символы в поле для ввода пароля
	$Control/your_password.set_secret(true)
	

# Вызывается при закрытии сцены
func _exit_tree():
	# Показываем глобальную сцену с меню
	Title.set_visible(true)
	
	
